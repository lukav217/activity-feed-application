create table if not exists users
(
    id                   uuid NOT NULL,
    username             varchar(50),
    password             varchar(50),
    email                varchar(50),
    first_name           varchar(50),
    last_name            varchar(50),
    role                 varchar(50),
    password_reset_token varchar(50),
    password_reset_time  timestamp
);

create unique index if not exists users_id_uindex
    on users (id);

create unique index if not exists users_username_uindex
    on users (username);

alter table users
    add constraint users_pk
        primary key (id);

INSERT INTO users(id, username, password, email, first_name, last_name, role, password_reset_token, password_reset_time)
VALUES ('d8c8fd06-4b71-11ec-81d3-0242ac130003', 'Pera', 'pass123', 'p@yahoo.com', 'Pera', 'lukic', 'admin', '', null);
INSERT INTO users(id, username, password, email, first_name, last_name, role, password_reset_token, password_reset_time)
VALUES ('8e7c9680-4b72-11ec-81d3-0242ac130003', 'Mika', 'pass123', 'm@yahoo.com', 'Mika', 'lukic', 'admin', '', null);
INSERT INTO users(id, username, password, email, first_name, last_name, role, password_reset_token, password_reset_time)
VALUES ('d2aadede-4b72-11ec-81d3-0242ac130003', 'Zika', 'pass123', 'z@yahoo.com', 'Zika', 'lukic', 'admin', '', null);
INSERT INTO users(id, username, password, email, first_name, last_name, role, password_reset_token, password_reset_time)
VALUES ('db7dbbb2-4b72-11ec-81d3-0242ac130003', 'Laza', 'pass123', 'l@yahoo.com', 'Laza', 'lukic', 'game manager', '', null);
INSERT INTO users(id, username, password, email, first_name, last_name, role, password_reset_token, password_reset_time)
VALUES ('e19a2102-4b72-11ec-81d3-0242ac130003', 'Jovan', 'pass123', 'j@yahoo.com', 'Jovan', 'lukic', 'game manager', '', null);
INSERT INTO users(id, username, password, email, first_name, last_name, role, password_reset_token, password_reset_time)
VALUES ('e95a80f8-4b72-11ec-81d3-0242ac130003', 'Bojan', 'pass123', 'b@yahoo.com', 'Bojan', 'lukic', 'admin', '', null);


create table if not exists games
(
    id          uuid not null,
    name        varchar(50),
    description varchar(50),
    launch_url  varchar(50),
    active      bool
);

create unique index if not exists games_id_uindex
    on games (id);

alter table games
    add constraint games_pk
        primary key (id);

INSERT INTO games(id, name, description, launch_url, active)
VALUES ('1c7adf43-b2b8-4a23-9e92-862260b07b7f', 'game 1', 'description of game 1', '/game1', true);
INSERT INTO games(id, name, description, launch_url, active)
VALUES ('7e67cb88-5b0e-4032-a528-4133909ad7bb', 'game 2', 'description of game 2', '/game2', false);
INSERT INTO games(id, name, description, launch_url, active)
VALUES ('08fdc494-4e4e-4086-9e83-40a5863dd329', 'game 3', 'description of game 3', '/game3', false);
INSERT INTO games(id, name, description, launch_url, active)
VALUES ('e4454681-a6a2-40ed-bf9b-74f0f3332c3b', 'game 4', 'description of game 4', '/game4', true);

create table if not exists user_activities
(
    id            uuid,
    activity_type varchar(50),
    created_by    uuid,
    created       timestamp,
    description   varchar(50)
);

create unique index if not exists user_activities_id_uindex
    on user_activities (id);

alter table user_activities
    add constraint user_activities_pk
        primary key (id);

create table "users_data"
(
    id uuid,
    role varchar(50)
);

create unique index "users-data_id_uindex"
    on "users_data" (id);

alter table "users_data"
    add constraint "users_data_pk"
        primary key (id);

INSERT INTO users_data(id,role) VALUES('d8c8fd06-4b71-11ec-81d3-0242ac130003', 'admin');
INSERT INTO users_data(id,role) VALUES('8e7c9680-4b72-11ec-81d3-0242ac130003', 'admin');
INSERT INTO users_data(id,role) VALUES('d2aadede-4b72-11ec-81d3-0242ac130003', 'admin');
INSERT INTO users_data(id,role) VALUES('db7dbbb2-4b72-11ec-81d3-0242ac130003', 'game manager');
INSERT INTO users_data(id,role) VALUES('e19a2102-4b72-11ec-81d3-0242ac130003', 'game manager');
INSERT INTO users_data(id,role) VALUES('e95a80f8-4b72-11ec-81d3-0242ac130003', 'admin');

create table if not exists password_resets
(
    id uuid,
    user_id uuid,
    token varchar(50),
    reset_time timestamp
);

create unique index if not exists password_resets_id_uindex
    on password_resets (id);

alter table password_resets
    add constraint password_resets_pk
        primary key (id);