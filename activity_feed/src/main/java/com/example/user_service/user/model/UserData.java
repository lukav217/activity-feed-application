package com.example.user_service.user.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Table(value = "users_data")
@Data
@NoArgsConstructor
public class UserData {
    @Id
    UUID id;

    String role;
}