package com.example.user_service.user.service;

import com.example.user_service.user.model.KafkaMessage;
import com.example.user_service.user.model.PasswordReset;
import com.example.user_service.user.repository.PasswordResetRepository;
import com.example.user_service.user.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
@AllArgsConstructor
public class PasswordResetsHandler {
    private final Logger logger = LoggerFactory.getLogger(UserEventHandler.class);

    PasswordResetRepository passwordResetRepository;
    UserRepository userRepository;
    ObjectMapper mapper;

    @KafkaListener(topics = "password-resets", groupId = "PasswordResetsHandler", containerFactory = "kafkaListener")
    public Mono<Void> passwordResetHandler(KafkaMessage message) throws IOException {
        logger.info(String.format("Pokupljeno sa password-resets topic %s", message.toString()));
        passwordResetRepository.save(mapper.convertValue(message.getBody(), PasswordReset.class)).subscribe();
        return Mono.empty();
    }
}
