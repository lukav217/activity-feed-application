package com.example.user_service.user.model;

public enum Status {
    CREATED,
    UPDATED,
    DELETED
}