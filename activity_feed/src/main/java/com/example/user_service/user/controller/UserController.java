package com.example.user_service.user.controller;

import com.example.user_service.user.model.User;
import com.example.user_service.user.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {
    private UserService userService;

    @PostMapping(value = "/register")
    public Mono<Void> register(@RequestBody User u) {
        return userService.createUser(u);
    }

    @GetMapping()
    public Mono<User> getUserById(@RequestParam("id") UUID id) {
        return userService.getUserById(id);
    }

    @GetMapping("/all")
    public Flux<User> getUsers(@RequestParam("id") UUID id, @RequestParam("page") int page, @RequestParam("size") int size) {
        return userService.getUsers(id, page, size);
    }

    @PostMapping("/edit")
    public Mono<Void> edit(@RequestBody User u) {
        return userService.updateUser(u);
    }
}
