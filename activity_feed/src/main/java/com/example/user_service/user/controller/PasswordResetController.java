package com.example.user_service.user.controller;

import com.example.user_service.user.model.PasswordReset;
import com.example.user_service.user.service.PasswordResetService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/password-reset")
@AllArgsConstructor
public class PasswordResetController {
    private PasswordResetService passwordResetService;

    @PostMapping()
    public String passwordResetRequest(@RequestBody PasswordReset passwordReset) {
        return passwordResetService.generatePasswordResetToken(passwordReset);
    }

    @GetMapping("/confirm")
    public Mono<Void> passwordResetConfirm(@RequestParam String token, @RequestBody() String pass) {
        return passwordResetService.passwordReset(token, pass);
    }
}
