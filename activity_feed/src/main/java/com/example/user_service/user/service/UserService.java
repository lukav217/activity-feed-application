package com.example.user_service.user.service;

import com.example.user_service.user.model.KafkaMessage;
import com.example.user_service.user.model.Status;
import com.example.user_service.user.model.User;
import com.example.user_service.user.repository.UserDataRepository;
import com.example.user_service.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private static final String TOPIC_USERS = "users";
    private static final String TOPIC_PASS = "password-resets";

    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    private UserRepository userRepository;
    private UserDataRepository userDataRepository;

    public Mono<Void> createUser(User u) {
        logger.info(String.format("Saljem na users topic: %s", u.toString()));
        this.kafkaTemplate.send(TOPIC_USERS, new KafkaMessage(Status.CREATED, u, u.getId(), LocalDateTime.now()));
        return Mono.empty();
    }

    public Mono<Void> updateUser(User user) {
        logger.info(String.format("Saljem na users topic: %s", user.toString()));
        this.kafkaTemplate.send(TOPIC_USERS, new KafkaMessage(Status.UPDATED, user, user.getId(), LocalDateTime.now()));
        return Mono.empty();
    }

    public Mono<User> getUserById(UUID id) {
        return userRepository.findById(id);
    }

    public Flux<User> getUsers(UUID id, int page, int size) {
        return userDataRepository
                .findById(id)
                .filter(user -> user.getRole().equals("admin"))
                .flatMapMany(userData -> userRepository.findAllBy(PageRequest.of(page, size)));
    }
}
