package com.example.user_service.user.service;

import com.example.user_service.user.model.KafkaMessage;
import com.example.user_service.user.model.User;
import com.example.user_service.user.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
@AllArgsConstructor
public class UserEventHandler {
    private final Logger logger = LoggerFactory.getLogger(UserEventHandler.class);

    private UserRepository userRepository;

    ObjectMapper mapper;

    @KafkaListener(topics = "users", groupId = "UserEventHandler", containerFactory = "kafkaListener")
    public Mono<Void> userKafkaListener(KafkaMessage message) throws IOException {
        logger.info(String.format("Pokupljeno sa users topic - create/edit: %s", message));
        userRepository.save(mapper.convertValue(message.getBody(), User.class)).subscribe();
        return Mono.empty();
    }


}
