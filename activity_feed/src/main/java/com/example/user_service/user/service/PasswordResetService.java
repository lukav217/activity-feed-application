package com.example.user_service.user.service;

import com.example.user_service.user.model.KafkaMessage;
import com.example.user_service.user.model.PasswordReset;
import com.example.user_service.user.model.Status;
import com.example.user_service.user.repository.PasswordResetRepository;
import com.example.user_service.user.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Service
@AllArgsConstructor
public class PasswordResetService {
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);
    private static final String TOPIC_PASS = "password-resets";

    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    private UserRepository userRepository;
    private PasswordResetRepository passwordResetRepository;

    private UserService userService;


    public String generatePasswordResetToken(PasswordReset passwordReset) {
        String token = UUID.randomUUID().toString();
        logger.info(String.format("Generisani token je:  %s", token));
        passwordReset.setToken(token);
        logger.info(String.format("Saljem na password-resets topic: %s", passwordReset.toString()));
        if ((passwordReset.getResetTime() == null))
            passwordReset.setResetTime(LocalDateTime.now());

        this.kafkaTemplate.send(TOPIC_PASS, new KafkaMessage(Status.CREATED, passwordReset, passwordReset.getId(), LocalDateTime.now()));
        return token;
    }

    public Mono<Void> passwordResetConfirm(PasswordReset passwordReset) {
        logger.info(String.format("Saljem na password-resets topic - confirm: %s", passwordReset.toString()));

        long hours = ChronoUnit.HOURS.between(LocalDateTime.now(), passwordReset.getResetTime());
        if (hours <= 1) {
            passwordReset.setResetTime(null);
            passwordReset.setToken(null);
        }
        this.kafkaTemplate.send(TOPIC_PASS, new KafkaMessage(Status.UPDATED, passwordReset, passwordReset.getId(), LocalDateTime.now()));
        return Mono.empty();
    }

    public Mono<Void> passwordReset(String token, String pass) {
        return passwordResetRepository.findPasswordResetByToken(token)
                .delayUntil(password -> userRepository.findById(password.getUserId())
                        .map(user -> user.setPassword(pass))
                        .flatMap(userService::updateUser))
                .flatMap(password -> this.passwordResetConfirm(password));
    }

}
