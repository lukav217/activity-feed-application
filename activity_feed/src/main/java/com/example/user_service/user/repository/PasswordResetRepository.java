package com.example.user_service.user.repository;

import com.example.user_service.user.model.PasswordReset;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface PasswordResetRepository extends R2dbcRepository<PasswordReset, UUID> {
    Mono<PasswordReset> findPasswordResetByToken(String token);
}
