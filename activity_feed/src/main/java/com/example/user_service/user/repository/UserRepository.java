package com.example.user_service.user.repository;

import com.example.user_service.user.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Repository
public interface UserRepository extends R2dbcRepository<User, UUID> {
    Flux<User> findAllBy(Pageable pageable);

    Mono<User> findUserByPasswordResetToken(String passwordResetToken);
}
