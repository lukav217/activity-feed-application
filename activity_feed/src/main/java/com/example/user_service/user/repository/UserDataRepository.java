package com.example.user_service.user.repository;

import com.example.user_service.user.model.UserData;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserDataRepository extends R2dbcRepository<UserData, UUID> {
}
