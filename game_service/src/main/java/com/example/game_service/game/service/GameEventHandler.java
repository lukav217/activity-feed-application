package com.example.game_service.game.service;

import com.example.game_service.game.model.Game;
import com.example.game_service.game.model.KafkaMessage;
import com.example.game_service.game.repository.GameRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.r2dbc.core.R2dbcEntityOperations;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Service
@AllArgsConstructor
public class GameEventHandler {
    private final Logger logger = LoggerFactory.getLogger(GameEventHandler.class);

    private R2dbcEntityOperations repositoryInsert;
    private GameRepository gameRepository;

    ObjectMapper mapper;

    @KafkaListener(topics = "games", groupId = "GameEventHandler", containerFactory = "gameListener")
    public Mono<Void> createAndUpdateUser(KafkaMessage message) throws IOException {
        logger.info(String.format("Pokupljeno sa games topic - create/edit: %s", message));
        gameRepository.save(mapper.convertValue(message.getBody(), Game.class)).subscribe();
        return Mono.empty();
    }
}
