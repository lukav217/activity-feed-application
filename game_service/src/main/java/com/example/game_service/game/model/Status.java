package com.example.game_service.game.model;

public enum Status {
    CREATED,
    UPDATED,
    DELETED
}
