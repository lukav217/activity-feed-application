package com.example.game_service.game.controller;

import com.example.game_service.game.model.Game;
import com.example.game_service.game.service.GameService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;


@RestController
@RequestMapping("/game")
@AllArgsConstructor
public class GameController {
    private GameService gameService;

    @PostMapping("/add")
    public Mono<Void> addNewGame(@RequestBody Game g) {
        return gameService.createGame(g);
    }

    @PostMapping("/edit")
    public Mono<Void> editGame(@RequestBody Game g) {
        return gameService.updateGame(g);
    }

    @GetMapping("/all")
    public Flux<Game> getGames(@RequestParam("page") int page, @RequestParam("size") int size) {
        return gameService.getAllGames(page, size);
    }

    @GetMapping()
    public Mono<Game> getUserById(@RequestParam("id") UUID id) {
        return gameService.getGameById(id);
    }


}
