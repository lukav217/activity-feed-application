package com.example.game_service.game.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class KafkaMessage {
    private Status status;
    private Object body;
    private UUID createdBy;
    private LocalDateTime createdAt;
}