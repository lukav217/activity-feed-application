package com.example.game_service.game.repository;

import com.example.game_service.game.model.Game;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface GameRepository extends R2dbcRepository<Game, UUID> {
    Flux<Game> findAllBy(Pageable pageable);
}
