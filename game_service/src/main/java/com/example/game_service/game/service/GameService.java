package com.example.game_service.game.service;

import com.example.game_service.game.model.Game;
import com.example.game_service.game.model.KafkaMessage;
import com.example.game_service.game.model.Status;
import com.example.game_service.game.repository.GameRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@AllArgsConstructor
public class GameService {
    private static final Logger logger = LoggerFactory.getLogger(GameService.class);
    private static final String TOPIC_GAMES = "games";

    private KafkaTemplate<String, KafkaMessage> kafkaTemplate;

    private GameRepository gameRepository;

    public Mono<Void> createGame(Game g) {
        logger.info(String.format("Saljem na games topic: %s", g.toString()));
        this.kafkaTemplate.send(TOPIC_GAMES, new KafkaMessage(Status.CREATED, g, g.getId(), LocalDateTime.now()));
        return Mono.empty();
    }

    public Mono<Void> updateGame(Game g) {
        logger.info(String.format("Saljem na games topic: %s", g.toString()));
        this.kafkaTemplate.send(TOPIC_GAMES, new KafkaMessage(Status.UPDATED, g, g.getId(), LocalDateTime.now()));
        return Mono.empty();
    }

    public Mono<Game> getGameById(UUID id) {
        return gameRepository.findById(id);
    }

    public Flux<Game> getAllGames(int page, int size) {
        return gameRepository.findAllBy(PageRequest.of(page, size));
    }
}
