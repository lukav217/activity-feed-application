package com.example.game_service.game.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Objects;
import java.util.UUID;

@Data
@Table(value = "games")
@NoArgsConstructor
public class Game implements Persistable<UUID> {
    @Id
    private UUID id;

    private String name;
    private String description;
    private String launchUrl;
    private boolean active;

    @Override
    public boolean isNew() {
        boolean result = Objects.isNull(id);
        this.id = result ? UUID.randomUUID() : this.id;
        return result;
    }
}
