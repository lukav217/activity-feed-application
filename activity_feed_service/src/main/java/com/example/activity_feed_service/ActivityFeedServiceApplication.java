package com.example.activity_feed_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivityFeedServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ActivityFeedServiceApplication.class, args);
    }

}
