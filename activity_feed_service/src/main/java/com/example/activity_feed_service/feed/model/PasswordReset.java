package com.example.activity_feed_service.feed.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Table(value = "password_resets")
@Data
@NoArgsConstructor
public class PasswordReset implements Persistable<UUID> {
    @Id
    private UUID id;

    private UUID userId;
    private String token;
    private LocalDateTime resetTime;


    @Override
    public boolean isNew() {
        boolean result = Objects.isNull(id);
        this.id = result ? UUID.randomUUID() : this.id;
        return result;
    }

}
