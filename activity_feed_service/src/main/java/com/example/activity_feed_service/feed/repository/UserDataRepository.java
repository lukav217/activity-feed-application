package com.example.activity_feed_service.feed.repository;

import com.example.activity_feed_service.feed.model.UserData;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface UserDataRepository extends R2dbcRepository<UserData, UUID> {
}
