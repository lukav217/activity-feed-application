package com.example.activity_feed_service.feed.service;

import com.example.activity_feed_service.feed.model.*;
import com.example.activity_feed_service.feed.repository.UserActivityRepository;
import com.example.activity_feed_service.feed.repository.UserDataRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class UserActivityEventHandler {
    private final Logger logger = LoggerFactory.getLogger(UserActivityEventHandler.class);

    private UserActivityRepository userActivityRepository;
    private UserDataRepository userDataRepository;

    ObjectMapper mapper;

    @KafkaListener(topics = "users", groupId = "UserActivityEventHandler", containerFactory = "userListener")
    public Mono<Void> createAndUpdateUser(KafkaMessage message) throws IOException {
        logger.info(String.format("Pokupljeno sa users topic - create/edit: %s", message));
        User u = mapper.convertValue(message.getBody(), User.class);

        UserActivity userActivity = new UserActivity();
        userActivity.setCreatedBy(u.getId());
        userActivity.setCreated(LocalDateTime.now());
        userActivity.setDescription("default desc");

        UserData userData = new UserData();
        userData.setUserId(u.getId());
        userData.setRole(u.getRole());

        if (message.getStatus() == Status.CREATED) {
            userActivity.setActivityType("created");
            userDataRepository.save(userData).subscribe();
        } else {
            userActivity.setActivityType("user info changed");
        }

        userActivityRepository.save(userActivity).subscribe();
        return Mono.empty();
    }

    @KafkaListener(topics = "password-resets", groupId = "UserActivityEventHandler", containerFactory = "userListener")
    public Mono<Void> passwordResetHandler(KafkaMessage message) throws IOException {
        logger.info(String.format("Pokupljeno sa password-resets topic %s", message));
        User u = mapper.convertValue(message.getBody(), User.class);

        UserActivity userActivity = new UserActivity();
        userActivity.setCreatedBy(u.getId());
        userActivity.setCreated(LocalDateTime.now());
        userActivity.setDescription("default desc");

        if (message.getStatus() == Status.CREATED) {
            userActivity.setActivityType("password reset request");
        } else {
            userActivity.setActivityType("password reset");
        }

        userActivityRepository.save(userActivity).subscribe();
        return Mono.empty();
    }


    @KafkaListener(topics = "games", groupId = "UserActivityEventHandler", containerFactory = "userListener")
    public Mono<Game> createAndUpdateGame(KafkaMessage message) throws IOException {
        logger.info(String.format("Pokupljeno sa games topic - create/edit: %s", message));
        Game g = mapper.convertValue(message.getBody(), Game.class);

        UserActivity userActivity = new UserActivity();
        userActivity.setCreatedBy(g.getId());
        userActivity.setCreated(LocalDateTime.now());
        userActivity.setDescription("default desc");

        if (message.getStatus() == Status.CREATED) {
            userActivity.setActivityType("game created");
        } else {
            userActivity.setActivityType("game updated");
        }

        userActivityRepository.save(userActivity).subscribe();
        return Mono.empty();
    }


}
