package com.example.activity_feed_service.feed.repository;

import com.example.activity_feed_service.feed.model.UserActivity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Repository
public interface UserActivityRepository extends R2dbcRepository<UserActivity, UUID> {
    Flux<UserActivity> findUserActivitiesByCreatedBy(UUID createdBy, Pageable pageable);
//    @Query(value = "SELECT * FROM user_activities WHERE user_activities.created_by = :created_by LIMIT :limit OFFSET :offset")
//    Flux<UserActivity> findUserActivitiesByCreated_by(@Param("created_by") UUID created_by, @Param("limit") int limit, @Param("offset") int offset);
}
