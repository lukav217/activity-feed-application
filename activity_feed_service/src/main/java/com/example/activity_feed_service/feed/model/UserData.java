package com.example.activity_feed_service.feed.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Objects;
import java.util.UUID;

@Table(value = "users_data")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserData implements Persistable<UUID> {
    @Id
    UUID id;

    UUID userId;
    String role;

    @Override
    public boolean isNew() {
        boolean result = Objects.isNull(id);
        this.id = result ? UUID.randomUUID() : this.id;
        return result;
    }
}
