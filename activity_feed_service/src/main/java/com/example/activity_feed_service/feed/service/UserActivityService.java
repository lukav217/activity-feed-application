package com.example.activity_feed_service.feed.service;

import com.example.activity_feed_service.feed.model.UserActivity;
import com.example.activity_feed_service.feed.repository.UserActivityRepository;
import com.example.activity_feed_service.feed.repository.UserDataRepository;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.UUID;

@Service
@AllArgsConstructor
public class UserActivityService {
    private static final Logger logger = LoggerFactory.getLogger(UserActivityService.class);

    private UserActivityRepository userActivityRepository;
    private UserDataRepository userDataRepository;

    public Flux<UserActivity> getUserActivities(UUID id, int page, int size) {
        return userDataRepository
                .findById(id)
                .filter(user -> user.getRole().equals("admin"))
                .flatMapMany(userData -> userActivityRepository.findUserActivitiesByCreatedBy(id, PageRequest.of(page, size)));
    }
}
