package com.example.activity_feed_service.feed.controller;

import com.example.activity_feed_service.feed.model.UserActivity;
import com.example.activity_feed_service.feed.service.UserActivityService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.UUID;

@RestController
@RequestMapping("/user-activity")
@AllArgsConstructor
public class UserActivityController {
    UserActivityService userActivityService;

    @GetMapping("")
    public Flux<UserActivity> getUserActivity(@RequestParam("id") UUID id, @RequestParam("page") int page, @RequestParam("size") int size) {
        return userActivityService.getUserActivities(id, page, size);
    }
}
