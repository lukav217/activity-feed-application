package com.example.activity_feed_service.feed.model;

public enum Status {
    CREATED,
    UPDATED,
    DELETED
}
